{
    description = "chpio-iobroker";

    inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs";
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, flake-utils, nixpkgs }:
        flake-utils.lib.eachDefaultSystem (system:
            let
                pkgs = import nixpkgs { inherit system; };
            in {
                devShell = pkgs.stdenv.mkDerivation {
                    name = "chpio-iobroker";
                    buildInputs = [ pkgs.nodejs-14_x];
                };
            }
        );
}
