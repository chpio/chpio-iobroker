export function simpleBrightnessToggle(
	buttonId: string,
	brightnessId: string,
	brightnessLevels: Array<number>,
): void {
	if (brightnessLevels.length === 0) {
		return;
	}

	on(buttonId, (buttonVal) => {
		if (buttonVal.state.val !== true) {
			return;
		}

		const nextBrightnessIndex = (() => {
			const brightness = getState<number>(brightnessId);
			if (brightness.val === null) {
				return 0;
			}
			let { index } = brightnessLevels
				.map((level) => Math.abs(level - brightness.val))
				.reduce(
					(acc, distance, index) => {
						if (distance < acc.distance) {
							return { distance, index };
						} else {
							return acc;
						}
					},
					{ distance: Number.MAX_VALUE, index: 0 },
				);
			index += 1;
			return index < brightnessLevels.length ? index : 0;
		})();

		setState(brightnessId, brightnessLevels[nextBrightnessIndex]!);
	});
}
